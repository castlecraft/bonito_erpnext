from setuptools import setup, find_packages

with open('requirements.txt') as f:
	install_requires = f.read().strip().split('\n')

# get version from __version__ variable in bonito_erpnext/__init__.py
from bonito_erpnext import __version__ as version

setup(
	name='bonito_erpnext',
	version=version,
	description='Custom App',
	author='Bonito',
	author_email='test@test.com',
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
