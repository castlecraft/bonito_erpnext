// Copyright (c) 2016, Bonito and contributors
// For license information, please see license.txt
/* eslint-disable */

frappe.query_reports["Bonito BOM Report"] = {
	"filters": [
		{
			"fieldname": "bom",
			"label": __("BOM"),
			"fieldtype": "Link",
			"options": "BOM",
			"reqd": 1
		},
	]
};
