# Copyright (c) 2013, Bonito and contributors
# For license information, please see license.txt

import frappe
from frappe import _


def execute(filters=None):
	if not filters: filters = {}

	columns = get_columns()

	data = get_bom_stock(filters)

	return columns, data

def get_columns():
	"""return columns"""
	columns = [
		_("BOM") + ":Link/BOM:150",
		_("Stock Entry") + ":Link/Stock Entry:150",
		_("Work Order") + ":Link/Work Order:150",
		_("Item") + "::150",
		_("Required Qty (BOM Qty)") + ":Float:160",
		_("BOM UoM") + "::160",
		_("Issued Qty") + ":Float:120",
		_("Pending Qty") + ":Float:120",
	]

	return columns

def get_bom_stock(filters):
	bom = filters.get("bom")

	return frappe.db.sql("""
			SELECT
				bom.name,
				se.name,
				se.work_order,
				se_item.item_code,
				bom_item.stock_qty,
				bom.uom,
				se_item.qty,
				bom_item.stock_qty - se_item.qty
			FROM
				`tabBOM` AS bom INNER JOIN `tabBOM Item` AS bom_item
					ON bom.name = bom_item.parent
				LEFT JOIN `tabStock Entry` AS se
					ON bom.name = se.bom_no
				INNER JOIN `tabStock Entry Detail` AS se_item
					ON se.name = se_item.parent and se_item.item_code = bom_item.item_code
			WHERE
				bom.name = {bom} and bom.docstatus != 2 and se.docstatus = 1
			
			ORDER BY se.name""".format(
				bom=frappe.db.escape(bom)
			))
