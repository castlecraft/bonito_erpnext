from frappe import _

def get_data():
	return [
		{
			"module_name": "Bonito Erpnext",
			"color": "grey",
			"icon": "octicon octicon-file-directory",
			"type": "module",
			"label": _("Bonito Erpnext")
		}
	]
