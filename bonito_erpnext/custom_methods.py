import frappe
from frappe import _

@frappe.whitelist(allow_guest=True)
def bom_validation(doc, method=None):
    if doc.stock_entry_type not in ["Material Issue","Material Receipt"]:
        bom = frappe.get_doc("BOM",doc.bom_no)
        
        for i in doc.items:

            for j in bom.items:
                
                if i.item_code == j.item_code and i.qty > j.qty:
                    frappe.throw(i.item_code + " Quantity Should Not Be Greated Than Bom Quantity")
  
  
                     